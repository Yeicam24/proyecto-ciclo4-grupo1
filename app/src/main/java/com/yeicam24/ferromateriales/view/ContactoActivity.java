package com.yeicam24.ferromateriales.view;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.view.map.MapsActivity;

public class ContactoActivity extends AppCompatActivity {

    private Button cali;
    private Button medellin;
    private Button bucaramanga;
    private Button bogota;
    private Button barranquilla;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacto);

        cali = findViewById(R.id.btn_cali);
        cali.setOnClickListener(v -> {
            Intent i = new Intent(ContactoActivity.this, MapsActivity.class);
            i.putExtra("ciudad", "Cali");
            i.putExtra("direccion", "Cra. 98 #16-200, Cali");
            startActivity(i);

        });

        bucaramanga = findViewById(R.id.btn_buaramanga);
        bucaramanga.setOnClickListener(v -> {
            Intent i = new Intent(ContactoActivity.this, MapsActivity.class);
            i.putExtra("ciudad", "Bucaramanga");
            i.putExtra("direccion", "Tv. 93 #34-99, Bucaramanga");
            startActivity(i);

        });

        medellin = findViewById(R.id.btn_medellin);
        medellin.setOnClickListener(v -> {
            Intent i = new Intent(ContactoActivity.this, MapsActivity.class);
            i.putExtra("ciudad", "Medellin");
            i.putExtra("direccion", "Cl. 54 ## 57-60, Medellín");
            startActivity(i);
        });
        bogota = findViewById(R.id.btn_bogota);
        bogota.setOnClickListener(v -> {
            Intent i = new Intent(ContactoActivity.this, MapsActivity.class);
            i.putExtra("ciudad", "Bogota");
            i.putExtra("direccion", "Dg. 40a #7 40, Bogotá");
            startActivity(i);
        });
        barranquilla = findViewById(R.id.btn_barranquilla);
        barranquilla.setOnClickListener(v -> {
            Intent i = new Intent(ContactoActivity.this, MapsActivity.class);
            i.putExtra("ciudad", "Barranquilla");
            i.putExtra("direccion", "Cl. 98 #52-115, Barranquilla");
            startActivity(i);

        });

    }
}