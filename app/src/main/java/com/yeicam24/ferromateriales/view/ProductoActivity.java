package com.yeicam24.ferromateriales.view;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.models.Productos;

public class ProductoActivity extends AppCompatActivity {
    ImageView  imgProducto;
    TextView   nombreProducto, descripcionProducto, precioProducto;
    Button     btnMas, btnMenos, btnCarrito;
    EditText   cantidad;
    Productos  producto;

    FirebaseDatabase db;
    DatabaseReference dbReferencia;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);

        imgProducto =  findViewById(R.id.img_Producto);
        nombreProducto = findViewById(R.id.nombreProducto);
        descripcionProducto = findViewById(R.id.descripcion_producto);
        precioProducto = findViewById(R.id.precio_producto);

        btnMas = findViewById(R.id.btn_mas);
        btnMenos = findViewById(R.id.btn_menos);
        btnCarrito = findViewById(R.id.btn_al_carrito);

        db = FirebaseDatabase.getInstance();
        dbReferencia = db.getReference();

        String idProducto = "PAKoAxFM220IjldYBn2m";

        dbReferencia.child("productos")
                .child(idProducto)
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                //producto = snapshot.getValue(Productos.class);
                //nombreProducto.setText(producto.getNombreProducto());


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


    }
}