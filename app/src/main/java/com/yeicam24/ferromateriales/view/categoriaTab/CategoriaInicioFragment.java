package com.yeicam24.ferromateriales.view.categoriaTab;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.adapters.CategoriaInicioAdapter;
import com.yeicam24.ferromateriales.models.CategoriaInicio;
import com.yeicam24.ferromateriales.view.ProductoActivity;

import java.util.ArrayList;

public class CategoriaInicioFragment extends Fragment {

    RecyclerView listaRecyclerView;
    ArrayList<CategoriaInicio> lista;
    CategoriaInicioAdapter mAdapter;
    FirebaseFirestore db;
    ProgressDialog anuncio;


    public CategoriaInicioFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_categoria_inicio, container, false);

        anuncio = new ProgressDialog(getContext());
        anuncio.setCancelable(false);
        anuncio.setMessage("Bienvenidos a su Ferreteria Carvajal Cargando datos...");
        anuncio.show();

        listaRecyclerView = vista.findViewById(R.id.listaProductos);

        listaRecyclerView.setHasFixedSize(true);
        listaRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));

        db = FirebaseFirestore.getInstance();
        lista = new ArrayList<CategoriaInicio>();
        mAdapter = new CategoriaInicioAdapter(getContext(), lista);

        listaRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ProductoActivity.class);
                startActivity(i);
            }
        });

        EventCangeListener();

        return vista;


    }

    private void EventCangeListener() {

        db.collection("productos")
                //.orderBy("nombreProducto", Query.Direction.ASCENDING)
                .addSnapshotListener((value, error) -> {
                    if (error != null) {
                        if (anuncio.isShowing())
                            anuncio.dismiss();
                        Log.e("Error de base de datos", error.getMessage());
                        return;
                    }
                    for (DocumentChange dc : value.getDocumentChanges()) {
                        if (dc.getType() == DocumentChange.Type.ADDED) {
                            lista.add(dc.getDocument().toObject(CategoriaInicio.class));
                        }
                        mAdapter.notifyDataSetChanged();
                        if (anuncio.isShowing())
                            anuncio.dismiss();
                    }
                });
    }

}