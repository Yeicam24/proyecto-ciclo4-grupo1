package com.yeicam24.ferromateriales.view.categoriaTab;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class CategoriaTab extends FragmentPagerAdapter {
    int numeroTabs;

    public CategoriaTab(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        this.numeroTabs = behavior;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                return new CategoriaInicioFragment();
            case 1:
                return new CategoriaHogarFragment();
            case 2:
                return new CategoriaElectricosFragment();
            case 3:
                return new CategoriaHerramientaFragment();
            case 4:
                return new CategoriaTuberiaFragment();
            case 5:
                return new CategoriaOtrosFragment();
            default:
                return null;

        }
    }

    @Override
    public int getCount() {
        return numeroTabs;
    }
}
