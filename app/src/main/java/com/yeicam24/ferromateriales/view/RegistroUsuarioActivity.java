package com.yeicam24.ferromateriales.view;


import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.view.navegacion.Navbar;

import java.util.HashMap;
import java.util.Map;

public class RegistroUsuarioActivity extends AppCompatActivity {

    private TextInputEditText nom_usuario;
    private TextInputEditText user_email;
    private TextInputEditText user_password;


    private String nameUser;
    private String emailUser;
    private String pass;


    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);

        nom_usuario = findViewById(R.id.nom_User);
        user_email = findViewById(R.id.user_email);
        user_password = findViewById(R.id.user_password);
/*
        Button btn_ir_al_login = findViewById(R.id.btn_ir_al_login);
        btn_ir_al_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistroUsuarioActivity.this, LoginActivity.class));

            }
        });
*/
        Button btn_register = findViewById(R.id.btn_register);
        btn_register.setOnClickListener(v -> {

            mAuth = FirebaseAuth.getInstance();
            mDatabase = FirebaseDatabase.getInstance().getReference();

            nameUser = nom_usuario.getText().toString();
            emailUser = user_email.getText().toString();
            pass = user_password.getText().toString();

            if(!nameUser.isEmpty() && !emailUser.isEmpty() && !pass.isEmpty()){
                if(pass.length()>=8){

                    registerUser();

                }else {
                    Toast.makeText(RegistroUsuarioActivity.this, "La Contraseña debe tener por lo meno 8 caracteres, ", Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(RegistroUsuarioActivity.this, "Debes completar todos los datos", Toast.LENGTH_SHORT).show();
            }

        });


    }
    private void registerUser() {

        mAuth.createUserWithEmailAndPassword(emailUser, pass).addOnCompleteListener(task -> {

            if(task.isSuccessful()){

                Map<String, Object> map = new HashMap<>();
                map.put("nombreUsuario", nameUser);
                map.put("emailUsuario", emailUser);
                map.put("password", pass);

                String id = mAuth.getCurrentUser().getUid();

                mDatabase.child("User").child(id).setValue(map).addOnCompleteListener(task2 -> {
                    if(task2.isSuccessful()){
                        startActivity(new Intent(RegistroUsuarioActivity.this, Navbar.class));
                        finish();
                    }else{
                        Toast.makeText(RegistroUsuarioActivity.this, "Los datos no pueden ser Creados ", Toast.LENGTH_SHORT).show();
                    }
                });

            }else {
                Toast.makeText(RegistroUsuarioActivity.this, "Este usuario no se ha podido Registrar ", Toast.LENGTH_SHORT ).show();
            }
        });
    }

}