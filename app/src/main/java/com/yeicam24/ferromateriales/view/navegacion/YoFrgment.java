package com.yeicam24.ferromateriales.view.navegacion;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.view.ContactoActivity;
import com.yeicam24.ferromateriales.view.login.LoginActivity;

public class YoFrgment extends Fragment {

    private TextView salir, ubicacion;
    private TextView usuario;
    private FirebaseAuth mAuth;
    private DatabaseReference usuario_db;


    public YoFrgment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_yo_frgment, container, false);

        mAuth = FirebaseAuth.getInstance();
        usuario_db = FirebaseDatabase.getInstance().getReference();
        usuario = vista.findViewById(R.id.aqui_nombre_usuario);


        salir = vista.findViewById(R.id.salir);
        salir.setOnClickListener(v -> {
            mAuth.signOut();
            startActivity(new Intent(getActivity(),LoginActivity.class));

        });

        ubicacion = vista.findViewById(R.id.ubicacion);
        ubicacion.setOnClickListener(v -> startActivity(new Intent(getActivity(), ContactoActivity.class)));

        getUserInfo();
        return vista;
    }

    private void getUserInfo(){
        String id = mAuth.getCurrentUser().getUid();
        usuario_db.child("User").child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    String nombreUsuario = snapshot.child("nombreUsuario").getValue().toString();
                    usuario.setText(nombreUsuario);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

}
