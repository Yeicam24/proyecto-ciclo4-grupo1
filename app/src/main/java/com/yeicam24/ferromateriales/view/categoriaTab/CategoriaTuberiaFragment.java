package com.yeicam24.ferromateriales.view.categoriaTab;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.adapters.CategoriaOtrosAdapter;
import com.yeicam24.ferromateriales.models.CategoriaOtros;

import java.util.ArrayList;

public class CategoriaTuberiaFragment extends Fragment {


    RecyclerView listaCategoriaTuberia;
    ArrayList<CategoriaOtros> listaDeCategoriaTuberia;

    public CategoriaTuberiaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_categoria_otros, container, false);

        listaDeCategoriaTuberia = new ArrayList<>();
        listaCategoriaTuberia = (RecyclerView) vista.findViewById(R.id.listaProductos);
        listaCategoriaTuberia.setLayoutManager(new GridLayoutManager(getContext(), 2));

        llenarLista();
        CategoriaOtrosAdapter adapter = new CategoriaOtrosAdapter(listaDeCategoriaTuberia);
        listaCategoriaTuberia.setAdapter(adapter);

        return vista;
    }

    private void llenarLista() {
        listaDeCategoriaTuberia.add(new CategoriaOtros("ADAPTADOR MACHO PRESION ","500","Adpatador macho para tuberia de agua , entre otras.",R.drawable.adaptador_macho_presion));
        listaDeCategoriaTuberia.add(new CategoriaOtros("ADAPTADOR PRESION HEMBRA ","500","Ideal Adapatador para tuberia de agua, etc, Fáciles de usar en el hogar, la oficina, fábricas, entre otras.",R.drawable.adaptador_presion_hembra));
        listaDeCategoriaTuberia.add(new CategoriaOtros("BUJE SOLDADO","2400","Union buje para tuberia sanitaria, usada para unir dos tubos de diferentes dimensiones.",R.drawable.buje_soldado));
        listaDeCategoriaTuberia.add(new CategoriaOtros("CODO SANITARIO PRESION","2200","Accesorio de tuberia de presion.",R.drawable.codo_sanitario_presion));
        listaDeCategoriaTuberia.add(new CategoriaOtros("CODO PVC SANITARIO","2400","Accesorios de tuberia sanitaria",R.drawable.codo_pvc_sanitario));
        listaDeCategoriaTuberia.add(new CategoriaOtros("SIFON FLEXIBLE GRIS","5000","Sifón flexible Facilita la utilización de sifón en condiciones especiales de instalación. Sifón plástico Elaborado en resina de alta ingeniería. Fácil instalación, limpieza y mantenimiento.",R.drawable.sifon_flexible_gris));
        listaDeCategoriaTuberia.add(new CategoriaOtros("LLAVE BOLA","1900","Controlar el paso del agua entre conductos Plásticos o Metalicos para la cocina, baños y exteriores",R.drawable.llave_bola));
        listaDeCategoriaTuberia.add(new CategoriaOtros("SIFON LAVAMANOS COMPLETO PLASTICO","5600","Reparaciones WC, tipo desagües, complemento de instalación de grifería, permite el adecuado desagüe, por si diseño evita los malos olores.",R.drawable.sifon_lavamanos_completo_plastico));
        listaDeCategoriaTuberia.add(new CategoriaOtros("TAPON ROSCADO PRESION","500","Accesorio de uso en tuberias de presion",R.drawable.tapon_roscado_presion));

    }
}