package com.yeicam24.ferromateriales.view.navegacion;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.yeicam24.ferromateriales.R;

public class Navbar extends AppCompatActivity {

    InicioFrgment inicioFrgment = new InicioFrgment();
    CarritoFrgment carritoFrgment = new CarritoFrgment();
    NovedadesFrgment novedadesFrgment = new NovedadesFrgment();
    YoFrgment yoFrgment = new YoFrgment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navbar);
        BottomNavigationView navegacion = findViewById(R.id.buttom_navigation);
        navegacion.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        loadFragment(inicioFrgment);
    }
    private final  BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()){
                case R.id.inicioFragment:
                    loadFragment(inicioFrgment);
                    return true;
                case R.id.carritoFragment:
                    loadFragment(carritoFrgment);
                    return true;
                case R.id.novedadesFragment:
                    loadFragment(novedadesFrgment);
                    return true;
                case R.id.yoFragment:
                    loadFragment(yoFrgment);
                    return true;
            }
            return false;
        }
    };
    public void loadFragment(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
    }
}