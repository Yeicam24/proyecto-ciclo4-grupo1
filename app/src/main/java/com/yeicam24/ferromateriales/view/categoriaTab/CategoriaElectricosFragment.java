package com.yeicam24.ferromateriales.view.categoriaTab;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.adapters.CategoriaElectricosAdapter;
import com.yeicam24.ferromateriales.models.CategoriaElectricos;


import java.util.ArrayList;

public class CategoriaElectricosFragment extends Fragment {

    RecyclerView listaCategoriaElectricos;
    ArrayList<CategoriaElectricos> listaDeCategoriaElectricos;

    public CategoriaElectricosFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_categoria_electricos, container, false);

        listaDeCategoriaElectricos = new ArrayList<>();
        listaCategoriaElectricos = (RecyclerView) vista.findViewById(R.id.listaProductos);
        listaCategoriaElectricos.setLayoutManager(new GridLayoutManager(getContext(), 2));

        llenarLista();
        CategoriaElectricosAdapter adapter = new CategoriaElectricosAdapter(listaDeCategoriaElectricos);
        listaCategoriaElectricos.setAdapter(adapter);

        return vista;
    }

    private void llenarLista() {
        listaDeCategoriaElectricos.add(new CategoriaElectricos("BOMBILLO LED 12W ","7200","Bombillo tipo led para uso domestco, ambienta espacios del hogar con una luz fria y ahorradora",R.drawable.img_bombillo_led_12w));
        listaDeCategoriaElectricos.add(new CategoriaElectricos("CLAVIJA GRADE 15AMP ","1900","Clavija resistente a grandes voltajes, alta durabilidad, patas de planas que remiten energía adecuada.",R.drawable.img_clavija_grade_15amp));
        listaDeCategoriaElectricos.add(new CategoriaElectricos("INTERUPTOR SENCILLO UDUKE ","3600","Interruptor doméstico, toma polo a tierra, resistente a la corrosión, de fácil limpieza e instalación.",R.drawable.img_interruptor_sencillo_uduke));
        listaDeCategoriaElectricos.add(new CategoriaElectricos("PANEL LED 18W REDONDO","15000","Panel led de alta calidad, de uso domestico para ambientar espacios",R.drawable.img_panel_led_18_watios_redondo));


    }


}