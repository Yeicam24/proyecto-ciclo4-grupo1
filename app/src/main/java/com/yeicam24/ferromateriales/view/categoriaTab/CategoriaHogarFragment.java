package com.yeicam24.ferromateriales.view.categoriaTab;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.adapters.CategoriaHogarAdapter;
import com.yeicam24.ferromateriales.models.CategoriaHogar;


import java.util.ArrayList;

public class CategoriaHogarFragment extends Fragment {

    RecyclerView listaCategoriaHogar;
    ArrayList<CategoriaHogar> listaDeCategoriaHogar;

    public CategoriaHogarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_categoria_hogar, container, false);

        listaDeCategoriaHogar = new ArrayList<>();
        listaCategoriaHogar = (RecyclerView) vista.findViewById(R.id.listaProductos);
        listaCategoriaHogar.setLayoutManager(new GridLayoutManager(getContext(), 2));

        llenarLista();
        CategoriaHogarAdapter adapter = new CategoriaHogarAdapter(listaDeCategoriaHogar);
        listaCategoriaHogar.setAdapter(adapter);

        return vista;
    }

    private void llenarLista() {
        listaDeCategoriaHogar.add(new CategoriaHogar("BROCHA BRUSH 2","3500","Brocha de calidad garantizada, con cerdas naturales, mango plástico negro y una perforación en la punta para su fácil almacenamiento.",R.drawable.img_brocha_brush_2));
        listaDeCategoriaHogar.add(new CategoriaHogar("GRIFO CROMADO UDUKE ","10000","Grifo para tanque, altamente resistente y duradero",R.drawable.img_grifo_cromado_uduke));
        listaDeCategoriaHogar.add(new CategoriaHogar("CANASTILLA LAVAPLATOS ","4500","canastilla de lujo, en material de alta resistencia a la humedad, no se pela, no se deforma con el calor, es más higiénico, cumple con las normas internacionales."
                ,R.drawable.img_canastilla_lavaplatos));
        listaDeCategoriaHogar.add(new CategoriaHogar("RODILLO GOYA BRICOLAGE","7000","Para pintar sobre todo tipo de superficies, para todo tipo de pinturas",R.drawable.img_rodillo_goya_bricolage));

    }
}