package com.yeicam24.ferromateriales.view.map;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.yeicam24.ferromateriales.R;

import java.util.List;
import java.util.Locale;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        Bundle datos = getIntent().getExtras();

        String ciudad = datos.getString("ciudad");
        String direccion = datos.getString("direccion");

        LatLng locations = new LatLng(4, -72);
        try {
            Geocoder geocoder = new Geocoder(MapsActivity.this.getApplicationContext(), Locale.getDefault());
            List<Address> addressList = geocoder.getFromLocationName(direccion, 1);
            if (!addressList.isEmpty()) {
                locations = new LatLng(addressList.get(0).getLatitude(),
                        addressList.get(0).getLongitude());
            }
        } catch (Exception e) {
            e.printStackTrace(); // getFromLocation() may sometimes fail
        }


        mMap.addMarker(new MarkerOptions().position(locations).title("Ferreteria Carvajal en " + ciudad + " " + direccion));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locations, 14f));
    }
}