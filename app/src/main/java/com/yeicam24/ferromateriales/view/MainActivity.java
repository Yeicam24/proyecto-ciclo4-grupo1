package com.yeicam24.ferromateriales.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
//import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.view.login.LoginActivity;
import com.yeicam24.ferromateriales.view.navegacion.Navbar;

public class MainActivity extends AppCompatActivity {
    ViewFlipper view_flipper;
    Button comenzarbutton;


    FirebaseAuth mAuth;
    DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        int[] images = { R.drawable.hogar, R.drawable.electricos, R.drawable.herramienta, R.drawable.tuberia, R.drawable.otros  };
        view_flipper = findViewById(R.id.view_flipper);

        //IniciarBoton, después cambiar
        comenzarbutton=findViewById(R.id.comenzar_button);
        comenzarbutton.setOnClickListener((evnt)->{
            Intent intent= new Intent(this, LoginActivity.class);
            startActivity(intent);
        });
        //Aquí acaba lo que debería borrar

        for(int image: images){
            flipperImages(image);

        }

    }

    public void flipperImages(int image){
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(image);

        view_flipper.addView(imageView);
        view_flipper.setFlipInterval(4000);
        view_flipper.setAutoStart(true);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        view_flipper.setInAnimation(this, android.R.anim.slide_out_right);
        view_flipper.setOutAnimation(this, android.R.anim.slide_in_left);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mAuth.getCurrentUser() != null) {
            startActivity(new Intent(MainActivity.this, Navbar.class));
        }
    }
}