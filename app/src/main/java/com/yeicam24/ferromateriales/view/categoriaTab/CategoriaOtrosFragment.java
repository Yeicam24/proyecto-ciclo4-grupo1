package com.yeicam24.ferromateriales.view.categoriaTab;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.adapters.CategoriaOtrosAdapter;
import com.yeicam24.ferromateriales.models.CategoriaOtros;

import java.util.ArrayList;


public class CategoriaOtrosFragment extends Fragment {

    RecyclerView listaCategoriaOtros;
    ArrayList<CategoriaOtros> listaDeCategoriaOtros;

    public CategoriaOtrosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_categoria_otros, container, false);

        listaDeCategoriaOtros = new ArrayList<>();
        listaCategoriaOtros = (RecyclerView) vista.findViewById(R.id.listaProductos);
        listaCategoriaOtros.setLayoutManager(new GridLayoutManager(getContext(), 2));

        llenarLista();
        CategoriaOtrosAdapter adapter = new CategoriaOtrosAdapter(listaDeCategoriaOtros);
        listaCategoriaOtros.setAdapter(adapter);

        return vista;
    }

    private void llenarLista() {
        listaDeCategoriaOtros.add(new CategoriaOtros("AMARRA BLANCA ","1600","Ideal para agrupar y organizar Cables, apretar mangueras, tareas de jardinería, etc, Fáciles de usar en el hogar, la oficina, fábricas, entre otras.",R.drawable.img_amarra));
        listaDeCategoriaOtros.add(new CategoriaOtros("AMARRA NEGRA ","1400","Ideal para agrupar y organizar Cables, apretar mangueras, tareas de jardinería, etc, Fáciles de usar en el hogar, la oficina, fábricas, entre otras.",R.drawable.img_amarra_negra));
        listaDeCategoriaOtros.add(new CategoriaOtros("CHAZO EXPANSION ","600","Chazo expansivo de alta resistencia para perforaciones en concreto.",R.drawable.img_chazos));
        listaDeCategoriaOtros.add(new CategoriaOtros("CHAZO PLASTICO","50","Chazo expansivo de alta resistencia para perforaciones en concreto.",R.drawable.img_chazo_plastico));
        listaDeCategoriaOtros.add(new CategoriaOtros("CHAZO PLASTICO CON TORNILLO","200","Chazo plastico resitente para perforaciones en concreto, madera, uso para cargas livianas",R.drawable.img_chazo_1_4));
        listaDeCategoriaOtros.add(new CategoriaOtros("CINTA 3M 10 Mtrs","3500","Aislante de uso profesional para cableados, en uniones, encintados y demás de uso general",R.drawable.img_cinta_negra));

    }
}