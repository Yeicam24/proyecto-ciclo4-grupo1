package com.yeicam24.ferromateriales.view.login;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.view.RegistroUsuarioActivity;
import com.yeicam24.ferromateriales.mvp.LoginMVP;
import com.yeicam24.ferromateriales.presenter.login.LoginPresenter;
import com.yeicam24.ferromateriales.view.navegacion.Navbar;

import java.util.Objects;


public class LoginActivity extends AppCompatActivity implements LoginMVP.View {

    private LinearProgressIndicator piWaiting;
    private TextInputLayout emailUsusario;
    private TextInputEditText etEmail;
    private TextInputLayout password;
    private TextInputEditText etPassword;

    private Button btnLogin;
    private FloatingActionButton btnRegistro;
    private FloatingActionButton btnFacebook;
    private FloatingActionButton btnGoogle;

    private LoginMVP.Presenter presenter;

    private String email="";
    private String clave="";

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        presenter = new LoginPresenter(this);

        initUI();
    }

    private void initUI() {
        piWaiting = findViewById(R.id.pi_waiting);

        emailUsusario = findViewById(R.id.emailUsuario);
        etEmail = findViewById(R.id.et_email);

        password = findViewById(R.id.password);
        etPassword = findViewById(R.id.et_password);


        btnRegistro = findViewById(R.id.fab_registro);
        btnRegistro.setOnClickListener(v -> startActivity(new Intent(LoginActivity.this, RegistroUsuarioActivity.class)));

        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(v -> {

            mAuth = FirebaseAuth.getInstance();

            email = etEmail.getText().toString();
            clave = etPassword.getText().toString();

            if (!email.isEmpty() && !clave.isEmpty()) {
                loginUser();
            } else {
                Toast.makeText(LoginActivity.this, "Es necesario completar los datos", Toast.LENGTH_SHORT).show();
            }

        });
    //btnLogin.setOnClickListener(evt -> presenter.loginWithEmail());

        btnFacebook = findViewById(R.id.fab_fb);
        btnFacebook.setOnClickListener(evt -> presenter.loginWithFacebook());

        btnGoogle = findViewById(R.id.fab_google);
        btnGoogle.setOnClickListener(evt -> presenter.loginWithGoogle());
    }
    private void loginUser(){
        mAuth.signInWithEmailAndPassword(email, clave).addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                startActivity(new Intent(LoginActivity.this, Navbar.class));
            }else {
                Toast.makeText(LoginActivity.this, "Correo o contraseña incorrectos", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public LoginMVP.LoginInfo getLoginInfo() {
        return new LoginMVP.LoginInfo(Objects.requireNonNull(etEmail.getText()).toString(), Objects.requireNonNull(etPassword.getText()).toString());
    }

    @Override
    public void showEmailError(String error) {
        emailUsusario.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        password.setError(error);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void clearData() {
        emailUsusario.setError("");
        etEmail.setText("");
        password.setError("");
        etPassword.setText("");
    }

    @Override
    public void openPaymentsActivity() {
        Intent intent = new Intent(this, Navbar.class);
        startActivity(intent);

    }

    @Override
    public void startWaiting() {
        piWaiting.setVisibility(View.VISIBLE);
        btnLogin.setEnabled(false);
        btnRegistro.setEnabled(false);
        btnFacebook.setEnabled(false);
        btnGoogle.setEnabled(false);
    }

    @Override
    public void stopWaiting() {
        piWaiting.setVisibility(View.GONE);
        btnLogin.setEnabled(true);
        btnRegistro.setEnabled(true);
        btnFacebook.setEnabled(true);
        btnGoogle.setEnabled(true);
    }


}