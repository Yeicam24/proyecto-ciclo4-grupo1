package com.yeicam24.ferromateriales.view.categoriaTab;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.adapters.CategoriaHerramientasAdapter;
import com.yeicam24.ferromateriales.models.CategoriaHerramientas;
import com.yeicam24.ferromateriales.models.CategoriaOtros;

import java.util.ArrayList;


public class CategoriaHerramentasFragment extends Fragment {

    RecyclerView listaCategoriaHerramientas;
    ArrayList<CategoriaHerramientas> listaDeCategoriaHerramientas;

    public CategoriaHerramentasFragment(){

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_categoria_herramenta, container, false);

        listaDeCategoriaHerramientas= new ArrayList<>();
        listaCategoriaHerramientas= (RecyclerView) vista.findViewById(R.id.listaProductos);
        listaCategoriaHerramientas.setLayoutManager(new GridLayoutManager(getContext(), 2));

        llenarListaHer();
        CategoriaHerramientasAdapter adapter = new CategoriaHerramientasAdapter(listaDeCategoriaHerramientas);
        listaCategoriaHerramientas.setAdapter(adapter);

        return vista;

    }

    private void llenarListaHer(){
        listaDeCategoriaHerramientas.add(new CategoriaHerramientas("BISTURI","2200","Herramienta ideal para corte de superficies como madera y plastico.",R.drawable.img_bisuri));
        listaDeCategoriaHerramientas.add(new CategoriaHerramientas("ESPATULA","2500","Fabricada en acero inoxidable, con mango antideslizante de polipropileno.",R.drawable.img_espatula_dos));
        listaDeCategoriaHerramientas.add(new CategoriaHerramientas("ESPATULA GOYA INOX","5000","Fabricada en acero inoxidable, con mango antideslizante de polipropileno",R.drawable.img_espatula));
        listaDeCategoriaHerramientas.add(new CategoriaHerramientas("LAPICES ROJOS","1000","Uso para carpinteria y construccion.",R.drawable.img_lapicesrojos));
        listaDeCategoriaHerramientas.add(new CategoriaHerramientas("LLANA DENTADA","10500","Llana lisa, resistente, duradera, fácil de usar.",R.drawable.img_llana_dentada));
        listaDeCategoriaHerramientas.add(new CategoriaHerramientas("LLANA LISA MANGO MADERA","18000","Llana lisa, resistente, duradera, fácil de usar.",R.drawable.img_llana_lisa_madera));
        listaDeCategoriaHerramientas.add(new CategoriaHerramientas("METRO 3MTS","4200","Flexómetro con botón de bloqueo de la cinta métrica, superficie de la cinta recubierta con Mylar, película plástica que protege la graduación hasta 10 veces más que las cintas barnizadas, gancho cero absoluto permite mayor precisión en las mediciones, resorte tratado a calor para una mayor vida útil del mecanismo que rebobina la cinta métrica, escala métrica y en pulgadas.",R.drawable.img_metro_3mts));
        listaDeCategoriaHerramientas.add(new CategoriaHerramientas("NIVEL ALUMINIO 12 UDUKE","12000","Se recomienda leer instrucciones de uso y utilizar elementos de protección personal.",R.drawable.img_nivel_aluminio));
        listaDeCategoriaHerramientas.add(new CategoriaHerramientas("PALUSTRE LUFKIN","8100","Palustre, de fácil y seguro agarre, resistente, fácil de usar.",R.drawable.img_palustre_lukfin));
        listaDeCategoriaHerramientas.add(new CategoriaHerramientas("PISTOLA MANGUERA 4 CHORROS UDUKE","5000","Pistola de riego con 4 funciones.",R.drawable.img_pistola_mangera_4chorros));
        listaDeCategoriaHerramientas.add(new CategoriaHerramientas("PUNTA TALADRO STANLEY","2000","Punta para taladro.",R.drawable.img_punta_taladro));
        listaDeCategoriaHerramientas.add(new CategoriaHerramientas("TIJERA LAMINA","12000","herramienta para corte de lamina.",R.drawable.img_tijera_lamina));
    }
}