package com.yeicam24.ferromateriales.view.navegacion;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.view.categoriaTab.CategoriaTab;

public class InicioFrgment extends Fragment {

    TabLayout tabLayout;
    ViewPager viewPager;
    CategoriaTab categoriaTab;

    public InicioFrgment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista  = inflater.inflate(R.layout.fragment_inicio_frgment, container, false);

        tabLayout = vista.findViewById(R.id.tab_layout);
        viewPager = vista.findViewById(R.id.viewPager);

        categoriaTab = new CategoriaTab(getActivity().getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(categoriaTab);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

                if(tab.getPosition() == 0){
                   categoriaTab.notifyDataSetChanged();
                }if(tab.getPosition() == 1){
                    categoriaTab.notifyDataSetChanged();
                }if(tab.getPosition() == 2){
                    categoriaTab.notifyDataSetChanged();
                }if(tab.getPosition() == 3){
                    categoriaTab.notifyDataSetChanged();
                }if(tab.getPosition() == 4){
                    categoriaTab.notifyDataSetChanged();
                }if(tab.getPosition() == 5){
                    categoriaTab.notifyDataSetChanged();
                }
            }


            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        //viewPager.addOnAdapterChangeListener( new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        return vista;

    }


}