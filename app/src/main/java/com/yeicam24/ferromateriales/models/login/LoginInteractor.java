package com.yeicam24.ferromateriales.models.login;

import com.yeicam24.ferromateriales.mvp.LoginMVP;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class LoginInteractor implements LoginMVP.Model {

    private final Map<String, String> users;

    public LoginInteractor(){
        users = new HashMap<>();
        users.put("yeison@correo.com", "12345678");
        users.put("test@email.com", "87654321");
        users.put("juan.pablo@correo.com", "12345678");
        users.put("juan.felipe@correo.com", "12345678");
        users.put("daniel@correo.com", "12345678");
        users.put("hugo@correo.com", "12345678");
    }

    @Override
    public void validateCredentials(String email, String password, ValidateCredentialsCallback callback) {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if(users.get(email) == null) {
            callback.onFailure("El Usuario no existe");
        } else if (!Objects.equals(users.get(email), password)){
            callback.onFailure("La Contraseña es incorrecta");
        } else {
            callback.onSuccess();
        }
    }
}

