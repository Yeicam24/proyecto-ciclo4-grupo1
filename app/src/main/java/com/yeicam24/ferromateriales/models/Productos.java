package com.yeicam24.ferromateriales.models;

public class Productos {
    private String nombreProducto;
    private Long precio;
    private String descripcion;
    private String url;
    private String id;

    public Productos() {
    }

    public Productos(String nombreProducto, Long precio, String descripcion, String url, String id) {

        this.nombreProducto = nombreProducto;
        this.precio = precio;
        this.descripcion = descripcion;
        this.url = url;
        this.id = id;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public Long getPrecio() {
        return precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getUrl() {
        return url;
    }

    public String getId() {
        return id;
    }
}
