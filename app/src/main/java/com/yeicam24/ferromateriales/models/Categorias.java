package com.yeicam24.ferromateriales.models;

public class Categorias {
    private String nombreCategoria;
    private int imagenCategoria;

    public Categorias() {
    }

    public Categorias(String nombreCategoria, int imagenCategoria) {
        this.nombreCategoria = nombreCategoria;
        this.imagenCategoria = imagenCategoria;

    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public int getImagenCategoria() {
        return imagenCategoria;
    }

    public void setImagenCategoria(int imagenCategoria) {
        this.imagenCategoria = imagenCategoria;
    }
}
