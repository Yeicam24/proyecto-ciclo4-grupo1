package com.yeicam24.ferromateriales.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.models.Categorias;

import java.util.List;

public class CategoriasAdapter extends RecyclerView.Adapter<CategoriasAdapter.ViewHolder> {
    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView nombreCategoria;
        private ImageView imagenCategoria;

        public ViewHolder(View itemView) {
            super(itemView);
            nombreCategoria = (TextView) itemView.findViewById(R.id.nombreCategoria);
            imagenCategoria = (ImageView) itemView.findViewById(R.id.imagenCategoria);
        }
    }
    public List<Categorias> listaCategorias;

    public CategoriasAdapter(List<Categorias> listaCategorias) {
        this.listaCategorias = listaCategorias;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_categorias, parent, false);
        ViewHolder viewHolder = new ViewHolder(vista);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.nombreCategoria.setText(listaCategorias.get(position).getNombreCategoria());
        holder.imagenCategoria.setImageResource(listaCategorias.get(position).getImagenCategoria());
    }

    @Override
    public int getItemCount() {
        return listaCategorias.size();
    }
}
