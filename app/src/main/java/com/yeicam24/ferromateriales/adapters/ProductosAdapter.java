package com.yeicam24.ferromateriales.adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.models.Productos;

import java.util.ArrayList;

public class ProductosAdapter extends RecyclerView.Adapter<ProductosAdapter.ViewHolder> {
   ArrayList<Productos> listaProducto;

    public  ProductosAdapter(ArrayList<Productos> listaProducto){
        this.listaProducto = listaProducto;
    }

    @NonNull
    @Override
    public ProductosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType ){
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_producto, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position){
        holder.nombreProducto.setText(listaProducto.get(position).getNombreProducto());
        holder.descripcionProducto.setText(listaProducto.get(position).getDescripcion());
        //holder.valorProducto.setText(listaProducto.get(position).getPrecio());
        //holder.imagenProducto.setImageResource(listaProducto.get(position).getUrl());
        //holder.imagenProducto.setImageURI(Uri.parse(listaProducto.get(position).getImagen()));
        /*
        URL url = null;
        try {
            url = new URL(listaProducto.get(position).getImagen());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Bitmap bmp = null;
        try {
            bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        holder.imagenProducto.setImageBitmap(bmp);
        */
    }


    @Override
    public int getItemCount(){
        return listaProducto.size();
    }
    public static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imagenProducto,imagenLike;
        TextView nombreProducto, descripcionProducto, valorProducto;
        CardView itemProducto;
        ViewHolder(View itemView){
            super(itemView);
            itemProducto = itemView.findViewById(R.id.itemProducto);
            imagenProducto = itemView.findViewById(R.id.imagenProducto);
            imagenLike = itemView.findViewById(R.id.imagenLike);
            nombreProducto = itemView.findViewById(R.id.nombreProducto);
            descripcionProducto = itemView.findViewById(R.id.descripcionProducto);
            valorProducto = itemView.findViewById(R.id.valorProducto);


        }
    }
}
