package com.yeicam24.ferromateriales.adapters;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.yeicam24.ferromateriales.view.login.RegistroTabFragment;
import com.yeicam24.ferromateriales.view.login.LoginTabFragment;

public class LoginTabAdapter extends FragmentPagerAdapter {
    private Context context;
    int totalTabs;


    public LoginTabAdapter(FragmentManager fm, Context context, int totalTabs){
        super(fm);
        this.context = context;
        this.totalTabs = totalTabs;

    }
    @Override
    public int getCount(){
        return  totalTabs;
    }
    public Fragment getItem(int position){
        switch (position){
            case 0:
                LoginTabFragment loginTabFragment = new LoginTabFragment();
                return loginTabFragment;
            case 1:
                RegistroTabFragment registroTabFragment = new RegistroTabFragment();
                return registroTabFragment;
            default:
                return null;


        }
    }
}

















