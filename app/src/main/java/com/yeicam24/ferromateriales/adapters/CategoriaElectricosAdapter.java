package com.yeicam24.ferromateriales.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.models.CategoriaElectricos;

import java.util.ArrayList;

public class CategoriaElectricosAdapter extends  RecyclerView.Adapter<CategoriaElectricosAdapter.ViewHolder> {

    ArrayList<CategoriaElectricos> listaCategoriasElectricos;

    public CategoriaElectricosAdapter(ArrayList<CategoriaElectricos> listaCategoriasElectricos){
        this.listaCategoriasElectricos = listaCategoriasElectricos;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_producto, parent, false);

        return new ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.nombreProducto.setText(listaCategoriasElectricos.get(position).getNombre());
        holder.descripcionProducto.setText(listaCategoriasElectricos.get(position).getDescripcion());
        holder.valorProducto.setText(listaCategoriasElectricos.get(position).getPrecio());
        holder.imagenProducto.setImageResource(listaCategoriasElectricos.get(position).getImagen());

    }

    @Override
    public int getItemCount() {
        return listaCategoriasElectricos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imagenProducto,imagenLike;
        TextView nombreProducto, descripcionProducto, valorProducto;
        CardView itemProducto;
        public ViewHolder(View itemView) {
            super(itemView);
            itemProducto = itemView.findViewById(R.id.itemProducto);
            imagenProducto = itemView.findViewById(R.id.imagenProducto);
            imagenLike = itemView.findViewById(R.id.imagenLike);
            nombreProducto = itemView.findViewById(R.id.nombreProducto);
            descripcionProducto = itemView.findViewById(R.id.descripcionProducto);
            valorProducto = itemView.findViewById(R.id.valorProducto);

        }
    }
}
