package com.yeicam24.ferromateriales.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.models.CategoriaInicio;

import java.util.ArrayList;

public class CategoriaInicioAdapter extends RecyclerView.Adapter<CategoriaInicioAdapter.ViewHolder>
implements View.OnClickListener{

    Context context;
    ArrayList<CategoriaInicio> lista;
    private  View.OnClickListener listener;

    public CategoriaInicioAdapter(Context context, ArrayList<CategoriaInicio> lista) {
        this.context = context;
        this.lista = lista;
    }

    @NonNull
    @Override
    public CategoriaInicioAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(context).inflate(R.layout.item_producto, parent, false);
        vista.setOnClickListener(this);
        return new ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriaInicioAdapter.ViewHolder holder, int position) {
        CategoriaInicio productos = lista.get(position);

        holder.nombreProducto.setText(productos.getNombreProducto());
        holder.descripcionProducto.setText(productos.getDescripcion());
        holder.valorProducto.setText(productos.getPrecio().toString());

        Log.i(CategoriaInicioAdapter.class.getSimpleName(), productos.getNombreProducto() + ": " + productos.getUrl());

        // mostrar imagen
        Glide.with(holder.imagenProducto)
                .load(productos.getUrl())
                .centerCrop()
                .into(holder.imagenProducto);
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }
    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener!= null){
            listener.onClick(view);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imagenProducto, imagenLike;
        TextView nombreProducto, descripcionProducto, valorProducto;
        CardView itemProducto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemProducto = itemView.findViewById(R.id.itemProducto);
            imagenProducto = itemView.findViewById(R.id.imagenProducto);
            imagenLike = itemView.findViewById(R.id.imagenLike);
            nombreProducto = itemView.findViewById(R.id.nombreProducto);
            descripcionProducto = itemView.findViewById(R.id.descripcionProducto);
            valorProducto = itemView.findViewById(R.id.valorProducto);

        }
    }
}
