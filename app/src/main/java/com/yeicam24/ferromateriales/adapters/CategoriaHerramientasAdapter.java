package com.yeicam24.ferromateriales.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.models.CategoriaHerramientas;
import com.yeicam24.ferromateriales.view.categoriaTab.CategoriaHerramentasFragment;

import java.util.ArrayList;

public class CategoriaHerramientasAdapter extends RecyclerView.Adapter<CategoriaHerramientasAdapter.ViewHolder>{
    ArrayList<CategoriaHerramientas> listaCategoriaHerramientas;

    public CategoriaHerramientasAdapter(ArrayList<CategoriaHerramientas> listaCategoriaHerramientas){
        this.listaCategoriaHerramientas= listaCategoriaHerramientas;
    }

    @NonNull
    @Override
    public CategoriaHerramientasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_producto, parent, false);

        return new ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.nombreProducto.setText(listaCategoriaHerramientas.get(position).getNombre());
        holder.descripcionProducto.setText(listaCategoriaHerramientas.get(position).getDescripcion());
        holder.valorProducto.setText(listaCategoriaHerramientas.get(position).getPrecio());
        holder.imagenProducto.setImageResource(listaCategoriaHerramientas.get(position).getImagen());
    }


    @Override
    public int getItemCount() {
        return listaCategoriaHerramientas.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imagenProducto,imagenLike;
        TextView nombreProducto, descripcionProducto, valorProducto;
        CardView itemProducto;
        public ViewHolder(View itemView) {
            super(itemView);
            itemProducto = itemView.findViewById(R.id.itemProducto);
            imagenProducto = itemView.findViewById(R.id.imagenProducto);
            imagenLike = itemView.findViewById(R.id.imagenLike);
            nombreProducto = itemView.findViewById(R.id.nombreProducto);
            descripcionProducto = itemView.findViewById(R.id.descripcionProducto);
            valorProducto = itemView.findViewById(R.id.valorProducto);

        }
    }


}
