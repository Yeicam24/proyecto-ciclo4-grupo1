package com.yeicam24.ferromateriales.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.yeicam24.ferromateriales.R;
import com.yeicam24.ferromateriales.models.CategoriaHogar;

import java.util.ArrayList;

public class CategoriaHogarAdapter extends RecyclerView.Adapter<CategoriaHogarAdapter.ViewHolder> {

    ArrayList<CategoriaHogar> listaCategoriasHogar;

    public CategoriaHogarAdapter(ArrayList<CategoriaHogar> listaCategoriasHogar){
        this.listaCategoriasHogar = listaCategoriasHogar;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_producto, parent, false);

        return new ViewHolder(vista);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.nombreProducto.setText(listaCategoriasHogar.get(position).getNombre());
        holder.descripcionProducto.setText(listaCategoriasHogar.get(position).getDescripcion());
        holder.valorProducto.setText(listaCategoriasHogar.get(position).getPrecio());
        holder.imagenProducto.setImageResource(listaCategoriasHogar.get(position).getImagen());

    }

    @Override
    public int getItemCount() {
        return listaCategoriasHogar.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imagenProducto,imagenLike;
        TextView nombreProducto, descripcionProducto, valorProducto;
        CardView itemProducto;
        public ViewHolder(View itemView) {
            super(itemView);
            itemProducto = itemView.findViewById(R.id.itemProducto);
            imagenProducto = itemView.findViewById(R.id.imagenProducto);
            imagenLike = itemView.findViewById(R.id.imagenLike);
            nombreProducto = itemView.findViewById(R.id.nombreProducto);
            descripcionProducto = itemView.findViewById(R.id.descripcionProducto);
            valorProducto = itemView.findViewById(R.id.valorProducto);

        }
    }
}